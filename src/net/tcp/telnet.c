
#include <stdio.h>
#include <ipxe/xfer.h>
#include <ipxe/features.h>
#include <ipxe/init.h>
#include <usr/ifmgmt.h>
#include <usr/route.h>
#include <ipxe/process.h>
#include <ipxe/posix_io.h>
#include <ipxe/iobuf.h>
#include <ipxe/tcp.h>
#include <ipxe/umalloc.h>

/** @file
 *
 * A demonstrator telnet client
 *
 */

#define MAX_TELNET_BUF 8000

extern struct tcp_connection* main_connection;

extern int tcp_xfer_deliver ( struct tcp_connection *tcp,
			      struct io_buffer *iobuf,
			      struct xfer_metadata *meta __unused );

extern char* readline (char *prompt); 

int telnet_open ( char* tcp_uri ) 
{
  int rc=0;
  int fd;
  int line_len = 0;
  int packet_len = 0;
  char* line;
  char transmit_buf[MAX_TELNET_BUF]; 
  userptr_t user_buf;

  fd = open(tcp_uri);
  user_buf = umalloc(MAX_TELNET_BUF);

  while(1)
  {
     while ((packet_len = read_user(fd,user_buf,0,MAX_TELNET_BUF)) > 0)
     {
        ((char *)user_to_phys(user_buf,0))[packet_len] = '\0';
        printf("%s", (char *)user_to_phys(user_buf,0));
     }
    
     line = readline(NULL);
     line_len = strlen(line);

     if (line_len < MAX_TELNET_BUF - 3)
     {   
        strcpy(transmit_buf, line);
        transmit_buf[line_len] = '\r';
        transmit_buf[line_len+1] = '\n';
     }
     else
        continue;
      
     struct io_buffer io;
     iob_populate(&io, (void *) transmit_buf, line_len+2, MAX_TELNET_BUF);
     tcp_xfer_deliver(main_connection, &io, NULL);
     step();
  }

	return rc;
}

static void close_all_netdevs ( void ) {
	struct net_device *netdev;

	for_each_netdev ( netdev ) {
		ifclose ( netdev );
	}
}

static int try_getting_telnet ( struct net_device *netdev, char* tcp_uri ) {

	/* Close all other network devices */
	close_all_netdevs();

	/* Open device and display device status */
	if ( ifopen ( netdev ) != 0 )
		goto err_ifopen;
	ifstat ( netdev );

	/* Configure device */
	if (ifconf ( netdev, NULL )!= 0 )
		goto err_dhcp;
	route();

  telnet_open(tcp_uri);

  return 0;
  err_dhcp:
  err_ifopen:
    return -1;
}

static void telnet_discover ( char* tcp_uri ) {
	struct net_device *netdev;

	for_each_netdev ( netdev ) {
		try_getting_telnet ( netdev, tcp_uri );
	}
}

void telnet ( char *tcp_uri )
{
  initialise();
  startup();
  telnet_discover(tcp_uri);
}
