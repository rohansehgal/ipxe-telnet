/*
 * Copyright (C) 2013 Michael Brown <mbrown@fensystems.co.uk>.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

FILE_LICENCE ( GPL2_OR_LATER );

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <ipxe/command.h>
#include <ipxe/parseopt.h>
#include <ipxe/timer.h>

/** @file
 *
 * telnet command
 *
 */

#define MAX_URI_LEN 2048

extern int telnet ( char* tcp_uri );

/** "telnet" options */
struct telnet_options {
	/** host name to resolve */
	char *host;
	/** port */
	unsigned int port;
};

/** "ping" option list */
static struct option_descriptor telnet_opts[] = {
	OPTION_DESC ( "server", 's', required_argument,
		      struct telnet_options, host, parse_string ),
	OPTION_DESC ( "port", 'p', required_argument,
		      struct telnet_options, port, parse_integer )
};

/** "ping" command descriptor */
static struct command_descriptor telnet_cmd =
	COMMAND_DESC ( struct telnet_options, telnet_opts, 0, 0, "" );

/**
 * The "ping" command
 *
 * @v argc		Argument count
 * @v argv		Argument list
 * @ret rc		Return status code
 */
static int telnet_exec ( int argc, char **argv ) {
	struct telnet_options opts;
	int rc;
  
  char tcp_uri[MAX_URI_LEN];
  //prevent overflows
  tcp_uri[MAX_URI_LEN - 1] = '\0';

	/* Initialise options */
	memset ( &opts, 0, sizeof ( opts ) );

	/* Parse options */
	if ( ( rc = reparse_options ( argc, argv, &telnet_cmd, &opts ) ) != 0 )
		return rc;

  snprintf(tcp_uri, MAX_URI_LEN-1, "tcp://%s:%d", opts.host, opts.port);
  
  telnet(tcp_uri);

	/* Parse hostname */
	return 0;
}

/** Ping command */
struct command telnet_command __command = {
	.name = "telnet",
	.exec = telnet_exec,
};
